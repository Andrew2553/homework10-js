// Отримуємо всі елементи вкладок і контенту
const tabTitles = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tabs-content li');

// Функція для переключення вкладок
function switchTab(event) {
  // Забираємо активний клас з усіх вкладок
  tabTitles.forEach(tab => tab.classList.remove('active'));

  // Додаємо активний клас для клікнутої вкладки
  event.target.classList.add('active');

  // Отримуємо індекс клікнутої вкладки
  const clickedIndex = Array.from(tabTitles).indexOf(event.target);

  // Перебираємо всі елементи контенту
  tabContents.forEach((content, index) => {
    // Перевіряємо, чи співпадають індекси вкладки і контенту
    if (index === clickedIndex) {
      // Показуємо контент для відповідної вкладки
      content.style.display = 'block';
    } else {
      // Приховуємо решту контенту
      content.style.display = 'none';
    }
  });
}

// Додаємо обробник подій для кожної вкладки
tabTitles.forEach(tab => {
  tab.addEventListener('click', switchTab);
});

// Функція для додавання нової вкладки
function addTab(title, content) {
  // Створюємо новий елемент вкладки
  const newTab = document.createElement('li');
  newTab.classList.add('tabs-title');
  newTab.textContent = title;

  // Створюємо новий елемент контенту
  const newContent = document.createElement('li');
  newContent.textContent = content;

  // Додаємо нові елементи до списків вкладок і контенту
  const tabsList = document.querySelector('.tabs');
  const contentList = document.querySelector('.tabs-content');
  tabsList.appendChild(newTab);
  contentList.appendChild(newContent);

  // Додаємо обробник подій для нової вкладки
  newTab.addEventListener('click', switchTab);
}

// Приклад використання: додавання нової вкладки
addTab('New Tab', 'This is the content of the new tab');

// Функція для видалення вкладки
function removeTab(index) {
  // Видаляємо елементи вкладки і контенту за вказаним індексом
  const tabsList = document.querySelector('.tabs');
  const contentList = document.querySelector('.tabs-content');
  tabsList.removeChild(tabsList.children[index]);
  contentList.removeChild(contentList.children[index]);

  // Перевіряємо, чи була видалена активна вкладка
  if (index === Array.from(tabTitles).indexOf(document.querySelector('.tabs-title.active'))) {
    // Якщо так, встановлюємо активною першу вкладку
    tabTitles[0].classList.add('active');
    tabContents[0].style.display = 'block';
  }
}

// Приклад використання: видалення другої вкладки
removeTab(1);
